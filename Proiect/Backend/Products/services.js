const {
    Products,
} = require('../data');

const getAll = async () => {
    return await Products.find();
};


const add = async (name, category, stock, price, img) => {
    const product = new Products({
        name,
        category,
        stock,
        price,
        img
    });
    await product.save();
};

const getById = async (id) => {
    return await Products.findById(id);
};

const updateById = async (id, stock) => {
    await Products.findByIdAndUpdate(id, {stock});
};

const deleteById = async (id) => {
    await Products.findByIdAndDelete(id);
};

module.exports = {
    getAll,
    add,
    getById,
    updateById,
    deleteById
}