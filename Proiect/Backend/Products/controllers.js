const express = require('express');

const ProductsService = require('./services.js')

const router = express.Router();

const {validateFields} = require('../utils');

const {authorizeAndExtractToken} = require('../security/Jwt');

const {authorizeRoles} = require('../security/Roles');

router.get('/', authorizeAndExtractToken, authorizeRoles('admin', 'user'), async (req, res, next) => {
    try {
        const products = await ProductsService.getAll();
        res.json(products);
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js 
        next(err);
    }
});


router.post('/', authorizeAndExtractToken, authorizeRoles('admin'), async (req, res, next) => {
    const {
        name,
        category,
        stock,
        img,
        price
    } = req.body;

    try {

        const fieldsToBeValidated = {
            name: {
                value: name,
                type: 'alpha'
            },
            category: {
                value: category,
                type: 'alpha'
            },
            stock: {
                value: stock,
                type: 'int'
            },
            price: {
                value: price,
                type: 'int'
            }
          
        };

        validateFields(fieldsToBeValidated);

        await ProductsService.add(name, category, stock, price, img);
        res.status(201).end();

    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js 
        next(err);
    }
});


router.get('/:id',authorizeAndExtractToken, authorizeRoles('admin', 'user'),  async (req, res, next) => {
    const {
        id
    } = req.params;
    try {
        validateFields({
            id: {
                value: id,
                type: 'ascii'
            }
        });
        const product = await ProductsService.getById(id);
        res.json(product);
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js 
        next(err);
    }
});

router.put('/:id', authorizeAndExtractToken, authorizeRoles('admin'), async (req, res, next) => {
    const {
        id
    } = req.params;

    const {
        stock
    } = req.body;

    try {

        const fieldsToBeValidated = {
            id: {
                value: id,
                type: 'ascii'
            },
            stock: {
                value: stock,
                type: 'int'
            }
        };

        validateFields(fieldsToBeValidated);

        await ProductsService.updateById(id, stock);
        res.status(204).end();
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js 
        next(err);
    }
});

router.delete('/:id', authorizeAndExtractToken, authorizeRoles('admin'), async (req, res, next) => {
    const {
        id
    } = req.params;

    try {

        validateFields ({
            id: {
                value: id,
                type: 'ascii'
            }
        });
        
        await ProductsService.deleteById(id);
        res.status(204).end();
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js 
        next(err);
    }
});

module.exports = router;