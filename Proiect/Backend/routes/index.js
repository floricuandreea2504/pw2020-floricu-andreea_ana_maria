const Router = require('express')();

const ProductsController = require('../Products/controllers.js');
const UsersController = require('../Users/controllers.js');
const MessagesController = require('../Messages/controllers.js');


Router.use('/products', ProductsController);
Router.use('/users', UsersController);
Router.use('/messages', MessagesController);


module.exports = Router;