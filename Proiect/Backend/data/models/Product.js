const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProductSchema = new Schema({
    name: String,
    category: String,
    stock: Number,
    price: Number,
    img: String
}, { timestamps: true });

const ProductModel = mongoose.model('Products', ProductSchema);
module.exports = ProductModel;
