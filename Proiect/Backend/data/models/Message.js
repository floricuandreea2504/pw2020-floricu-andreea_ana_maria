const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const MessageSchema = new Schema({
    name: String,
    email: String,
    message_content: String,
    important: Boolean,
    response: String
}, { timestamps: true });

const MessageModel = mongoose.model('Messages', MessageSchema);
module.exports = MessageModel;
