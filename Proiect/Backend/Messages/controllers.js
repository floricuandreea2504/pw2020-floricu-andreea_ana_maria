const express = require('express');

const MessagesService = require('./services.js')

const router = express.Router();

const {authorizeAndExtractToken} = require('../security/Jwt');

const {authorizeRoles} = require('../security/Roles');

router.get('/', authorizeAndExtractToken, authorizeRoles( 'user', 'suport'), async (req, res, next) => {
    try {
        const messages = await MessagesService.getAll();
        res.json(messages);
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js 
        next(err);
    }
});


router.post('/', authorizeAndExtractToken, authorizeRoles('user', 'suport'), async (req, res, next) => {
    const {
        name,
        address,
        message_content,   
    } = req.body;
    try {

        await MessagesService.add(name, address, message_content);
        res.status(201).end();

    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js 
        next(err);
    }
});

router.put('/:id', authorizeAndExtractToken, authorizeRoles('suport'), async (req, res, next) => {
    const {
        id
    } = req.params;

    const {
        response
    } = req.body;

    try {


        await MessagesService.updateById(id, response);
        res.status(204).end();
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js 
        next(err);
    }
});


module.exports = router;