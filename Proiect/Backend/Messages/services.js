const {
    Messages,
} = require('../data');

const getAll = async () => {
    return await Messages.find();
};


const add = async (name, address, message_content) => {
    const message = new Messages({
        name,
        address,
        message_content,
        important: false,
        response: ""
    });
    await message.save();
};


const updateById = async (id, response) => 
{   let important = true;
    await Messages.findByIdAndUpdate(id, {response, important});
};

module.exports = {
    getAll,
    add,
    updateById
}