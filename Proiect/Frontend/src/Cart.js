import React from 'react';
import { NavLink} from 'react-router-dom';

const taburiNelogat = [  { name: "Acasă",
                            link: "/"},
                        { name: "Înregistrare",
                            link: "/register"},
                        { name: "Intră în cont",
                            link: "/login"}
                        ];


const taburiAdmin = [  { name: "Acasă",
                            link: "/"},
                            { name: "Magazin",
                            link: "/products"},
                            { name: "Administrare",
                            link : "/admin"}
                            ];


const taburiUser = [  { name: "Acasă",
                            link: "/"},
                            { name: "Magazin",
                            link: "/products"},
                            { name: "Contact",
                              link: "/contact"},
                            {name: "FAQ",
                            link: "/faq"}
                            ];

class Cart extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            address: "",
            livrare : "",

        };

        this.subQuant = this.subQuant.bind(this);
        this.addQuant = this.addQuant.bind(this);
    }

    handleInput = event => {
        const { name, value } = event.target;
    
        this.setState({
          [name]: value,
        });
    };


    check_browser = () => {
        if ('localStorage' in window && window['localStorage'] !== null) {
            // We can use localStorage object to store data.
            //localStorage.setItem('cart', "[]");
            return true;
        } else {
                return false;
        }
    } 

    subQuant = (name) => {
        if (this.check_browser) {
            var cart = JSON.parse(localStorage.getItem('cart'));

            for (var i = 0; i < cart.length; i++) {
                if (cart[i].name.localeCompare(name) === 0) {
                    if (cart[i].quant !== 0) {
                        cart[i].quant--;
                    }

                    localStorage.setItem('cart', JSON.stringify(cart));
                    window.location.reload(false);
                    return;
                } 
            }
        }

    }

    addQuant = (name) => {
        if (this.check_browser) {
            var cart = JSON.parse(localStorage.getItem('cart'));

            for (var i = 0; i < cart.length; i++) {
                if (cart[i].name.localeCompare(name) === 0) {
                    cart[i].quant++;

                    localStorage.setItem('cart', JSON.stringify(cart));
                    window.location.reload(false);
                    return;
                } 
            }
        }
    }

    sendCommand = () => {
        alert("va urma");
    }

    tabsToRender = () => {  
        if (localStorage.getItem('userLogged') != null) {
           if (localStorage.getItem('userLogged').localeCompare("admin") === 0) {
                return (
                <ul id = "taburi">
                    {taburiAdmin.map(function(item) {
                    return <li key={item.name}>{<NavLink to={item.link}>{item.name}</NavLink>}</li>
                    })}
                </ul>)
    
            } else {
                return (
                <ul id = "taburi">
                    {taburiUser.map(function(item) {
                    return <li key={item.name}>{<NavLink to={item.link}>{item.name}</NavLink>}</li>
                    })}
                </ul>)
            }
        } else {
            return (
            <ul id = "taburi">
                {taburiNelogat.map(function(item) {
                return <li key={item.name}>{<NavLink to={item.link}>{item.name}</NavLink>}</li>
                })}
            </ul>)
        }
    }

    toRender = () => {
        if (localStorage.getItem('userLogged') === null) {
            return <h2 className="left"> Intra in cont pentru cumparaturi :) </h2>
    
        }
        return <div className="hello">
        <h3 className="left">Hello, {localStorage.getItem('userLogged')}!</h3>
        <button type='button' className="right" onClick={this.onLogout}>Logout</button>
        </div>
    }

    onLogout = () => {
        localStorage.clear();
        this.props.history.push('/');
        this.props.history.go();
    };

    render() {
        if (localStorage.getItem('cart') != null) {
        let cartList = JSON.parse(localStorage.getItem('cart')).map(item => {
            return (
                <div className="cos" key={item.name}>
                    { <img src={require(`${item.img}`)} alt="images" className="images"/>    }
                    <p> {item.name}</p>
                    <p id="quant_text"> Cantitate: </p>

                    <button type="button" id={item.name} className="button_minus_cart"
                    onClick={
                        () => this.subQuant(item.name)
                    }>-</button>

                    <p id="cart_quant">{item.quant}</p>

                    <button type="button" id={item.name} className="button_plus_cart"
                    onClick={
                        () => this.addQuant(item.name)
                    }>+</button>

                    <p id="cart_price"> Preț: {item.price * item.quant}</p>
                 </div>
            );
            });

        var totalPrice = 0;
        var items = JSON.parse(localStorage.getItem('cart'))

        for (var i = 0; i < items.length; i++) {
            totalPrice += items[i].price * items[i].quant;
        }

        return <div>
           <svg className="logo" height="130" width="400">
                    <defs>
                        <linearGradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
                        <stop offset="0%" id="stop-o1"/>
                        <stop offset="100%" id="stop-o2"/>
                        </linearGradient>
                    </defs>
                    <ellipse cx="200" cy="70" rx="185" ry="55" fill="url(#grad1)" />
                    <text fill="#ffffff" fontSize="45" fontFamily="Verdana"
                    x="50" y="86">Bijutieria ADA</text>
                </svg>
                <this.toRender/>

                <this.tabsToRender/>    

            <h2 id="cos_title">Coș de cumpărături</h2>

            <div>
                {cartList}
            </div>

            <div>
                <h2>Prețul total al comenzii: {totalPrice} lei</h2>
            </div>
            <form>
                    <div>
                        <label>
                            <b>Nume si prenume</b>
                            <input type="text" className="login-input" name="name" value={this.state.name} 
                            onChange={this.handleInput}/>
                        </label>
                    </div>
                    <div>
                        <label>
                            <b>Adresa email</b>
                            <input type="text" name="address" className="login-input" value={this.state.address}
                            onChange={this.handleInput}/>
                        </label>
                    </div>
                    <div>
                        <label>
                            <b>Adresa de livrare</b>
                            <input type="text" className="login-input" name="livrare" value={this.state.username} 
                            onChange={this.handleInput}/>
                        </label>
                    </div>
                </form>


            <button type='button' className="send_cart_button" onClick={this.sendCommand}>Trimite comanda</button>

        </div>
    } else {
            return <h2 className="left"> Nu ai niciun produs în coș </h2>

    }
}
}

export default Cart;