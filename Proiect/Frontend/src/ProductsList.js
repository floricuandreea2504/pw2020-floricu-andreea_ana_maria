import React from 'react';
import axios from 'axios';
import { NavLink} from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShoppingBasket } from '@fortawesome/free-solid-svg-icons';


const taburiNelogat = [  { name: "Acasă",
                            link: "/"},
                        { name: "Înregistrare",
                            link: "/register"},
                        { name: "Intră în cont",
                            link: "/login"}
                        ];


const taburiAdmin = [  { name: "Acasă",
                            link: "/"},
                            { name: "Magazin",
                            link: "/products"},
                            { name: "Administrare",
                            link : "/admin"}
                            ];


const taburiUser = [  { name: "Acasă",
                            link: "/"},
                            { name: "Magazin",
                            link: "/products"},
                            { name: "Contact",
                              link: "/contact"},
                            {name: "FAQ",
                            link: "/faq"}
                            ];


class ProductsList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            items: [],
            name: '',
            category: '',
        };
    }

    componentDidMount() {
        this.getProducts();
      }


    onLogout = () => {
        localStorage.clear();
        this.props.history.push('/');
        this.props.history.go();
    };

    add_to_cart = (img, nume, price) => {

        var cart = JSON.parse(localStorage.getItem('cart'));

        if (cart == null) {
            cart = [{
                img : img,
                name : nume,
                price : price,
                quant : 1       
            }];
            this.componentDidMount();
        } else {
            for (var i = 0; i < cart.length; i++) {
                if (cart[i].name.localeCompare(nume) === 0) {
                    cart[i].quant++;
                    localStorage.setItem('cart', JSON.stringify(cart));
                    return;
                }
            }

            cart.push({
                img : img,
                name : nume,
                price : price,
                quant : 1
            });
        } 

        localStorage.setItem('cart', JSON.stringify(cart));
    }

    tabsToRender = () => {  
        if (localStorage.getItem('userLogged') != null) {
           if (localStorage.getItem('userLogged').localeCompare("admin") === 0) {
                return (
                <ul id = "taburi">
                    {taburiAdmin.map(function(item) {
                    return <li key={item.name}>{<NavLink to={item.link}>{item.name}</NavLink>}</li>
                    })}
                </ul>)
    
            } else {
                return (
                <ul id = "taburi">
                    {taburiUser.map(function(item) {
                    return <li key={item.name}>{<NavLink to={item.link}>{item.name}</NavLink>}</li>
                    })}
                </ul>)
            }
        } else {
            return (
            <ul id = "taburi">
                {taburiNelogat.map(function(item) {
                return <li key={item.name}>{<NavLink to={item.link}>{item.name}</NavLink>}</li>
                })}
            </ul>)
        }
    }

    toRender = () => {
        if (localStorage.getItem('userLogged') === null) {
            return <h2 className="left"> Intra in cont pentru cumparaturi :) </h2>
    
        }
        return <div className="hello">
        <h3 className="left">Hello, {localStorage.getItem('userLogged')}!</h3>
        <button type='button' className="right" onClick={this.onLogout}>Logout</button>
        </div>
    }

    handleInput = event => {
        const { name, value } = event.target;
    
        this.setState({
          [name]: value,
        });
    };

    getProducts = () => {
        axios.get('http://localhost:3000/api/v1/products', {
            headers: {
                Authorization: `Bearer: ${localStorage.getItem('token')}`
            }
        })
        .then(response => {
            console.log(response.data)
            this.setState({ items: response.data });
        })
        .catch(error => console.log(error));
    };

    render(){
        let itemList = this.state.items.map(item=>{
            return(
                <div className="card-container" key={item._id}>
                        <div className="card-image">
                            <img src={require(`${item.img}`)} className="images" alt="images"/> 
                            <h1> {item.name}</h1>
                        </div>

                        <div className="card-content">
                            <p><b>Stoc: </b>{item.stock}</p>
                            <p><b>Pret: {item.price} $</b></p>
                        </div>
                        <button id={item.img} value={item.price} name={item.name} type="button" onClick={e => this.add_to_cart(e.target.id, e.target.name, e.target.value)}>Adaugă în coș</button>

                 </div> 
            )
        })

        return(
            <div className="container">
                <svg className="logo" height="130" width="400">
                    <defs>
                        <linearGradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
                        <stop offset="0%" id="stop-o1"/>
                        <stop offset="100%" id="stop-o2"/>
                        </linearGradient>
                    </defs>
                    <ellipse cx="200" cy="70" rx="185" ry="55" fill="url(#grad1)" />
                    <text fill="#ffffff" fontSize="45" fontFamily="Verdana"
                    x="50" y="86">Bijutieria ADA</text>
                </svg>

                <this.toRender/>
                <button type="button" className="cart_icon">{<NavLink to={'/cart'}><FontAwesomeIcon icon={faShoppingBasket} size="2x"/></NavLink>}
                        </button>
                <this.tabsToRender/>    
                
                <div className="box">
                    {itemList}
                </div>
            </div>
        )
    }
}
    

export default ProductsList;

