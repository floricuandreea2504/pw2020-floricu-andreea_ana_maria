import React from 'react';
import './App.scss';
import { HashRouter, Switch, Route} from 'react-router-dom';
import Authentication from './Components/Authentication'
import ProductsList from './ProductsList';
import Register from './Components/Register';
import Administration from './Components/Administration';
import Home from './Components/Home';
import { history} from './history';
import Contact from './Components/Contact';
import FAQ from './Components/FAQ';
import FAQSuport from './Components/FAQSuport';
import Cart from './Cart';




function App() {
  return (
    <HashRouter  history={history}>
      <Switch>
        <Route exact path={'/login'} component={Authentication} />
        <Route exact path={'/register'} component={Register} />
        <Route exact path={'/'} component={Home} />
        <Route exact path={'/products'} component={ProductsList} />
        <Route exact path={'/admin'} component={Administration} />
        <Route exact path={'/contact'} component={Contact} />
        <Route exact path={'/faq'} component={FAQ} />
        <Route exact path={'/faqSuport'} component={FAQSuport} />
        <Route exact path={'/cart'} component={Cart} />
    </Switch>  
  </HashRouter>
);
}
export default App;

