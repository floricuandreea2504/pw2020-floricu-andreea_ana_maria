import React from 'react';
import axios from 'axios';
import { NavLink} from 'react-router-dom';


const taburiNelogat = [  { name: "Acasă",
                            link: "/"},
                        { name: "Înregistrare",
                            link: "/register"},
                        { name: "Intră în cont",
                            link: "/login"}
                        ];


const taburiAdmin = [  { name: "Acasă",
                            link: "/"},
                            { name: "Magazin",
                            link: "/products"},
                            { name: "Administrare",
                            link : "/admin"}
                            ];

const taburiUser = [  { name: "Acasă",
                            link: "/"},
                            { name: "Magazin",
                            link: "/products"},
                            { name: "Contact",
                              link: "/contact"},
                            {name: "FAQ",
                            link: "/faq"}
                            ];

                            
const taburiSuport = [ { name: "FAQ-Suport",
                        link : "/faqSuport"
                        },
                        { name: "Acasă",
                        link: "/"},
                    ];

class FAQSuport extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            messages: [],
            response: '',
        };
    }

    componentDidMount() {
        this.getMessages();
    }

    getMessages = () => {
        axios.get('http://localhost:3000/api/v1/messages', {
            headers: {
                Authorization: `Bearer: ${localStorage.getItem('token')}`
            }
        })
        .then(response => {
            console.log(response.data)
            this.setState({ messages: response.data });
        })
        .catch(error => console.log(error));
    };

    onLogout = () => {
        localStorage.clear();
        this.props.history.push('/');
        this.props.history.go();
    };

    handleInput = event => {
    const { name, value } = event.target;

    this.setState({
      [name]: value,
    });
};

    tabsToRender = () => {  
        if (localStorage.getItem('userLogged') != null) {
           if (localStorage.getItem('userLogged').localeCompare("admin") === 0) {
                return (
                <ul id = "taburi">
                    {taburiAdmin.map(function(item) {
                    return <li key={item.name}>{<NavLink to={item.link}>{item.name}</NavLink>}</li>
                    })}
                </ul>)
            } else if (localStorage.getItem('userLogged').localeCompare("suport") === 0) {
            return (
                <ul id = "taburi">
                    {taburiSuport.map(function(item) {
                    return <li key={item.name}>{<NavLink to={item.link}>{item.name}</NavLink>}</li>
                    })}
                </ul>)
            } else {
                return (
                <ul id = "taburi">
                    {taburiUser.map(function(item) {
                    return <li key={item.name}>{<NavLink to={item.link}>{item.name}</NavLink>}</li>
                    })}
                </ul>)
            }
            
        } else {
            return (
            <ul id = "taburi">
                {taburiNelogat.map(function(item) {
                return <li key={item.name}>{<NavLink to={item.link}>{item.name}</NavLink>}</li>
                })}
            </ul>)
    }
}

toRender = () => {
    if (localStorage.getItem('userLogged') === null) {
        return <h2 className="left"> Intra in cont pentru cumparaturi :) </h2>

    }
    return <div className="hello">
    <h3 className="left">Hello, {localStorage.getItem('userLogged')}!</h3>
    <button type='button' className="right" onClick={this.onLogout}>Logout</button>
    </div>
}

postResponse = (id, response) => {

    axios.put(`http://localhost:3000/api/v1/messages/${id}`,
    {
        response: response,
        important: true
    },
    {
        headers: {
            Authorization: `Bearer: ${localStorage.getItem('token')}`
        }
    })
    .then(response => {
        alert('Raspuns trimis');
        this.getMessages();
    })
    .catch(error => { alert(error.message) });

};


render(){
    let messagesList = this.state.messages.map(item=>{
        if (item.important === false)
        return(
            <div className="message" key={item._id}> 

                <h4>Q: {item.message_content}</h4>
                <div>
                    <b>Answer:</b>
                    <textarea rows="10" cols="100" name="response" className="input-response"
                    onChange={this.handleInput}/>
                <button className="button-basic" type='button' id={item._id}  value={this.state.response}
                onClick={e => this.postResponse(e.target.id, e.target.value)}>Trimite raspuns</button>
                 </div>
             </div>  
        )
    })

    return(
        <div className="container">
            <svg className="logo" height="130" width="400">
                <defs>
                    <linearGradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
                    <stop offset="0%" id="stop-o1"/>
                    <stop offset="100%" id="stop-o2"/>
                    </linearGradient>
                </defs>
                <ellipse cx="200" cy="70" rx="185" ry="55" fill="url(#grad1)" />
                <text fill="#ffffff" fontSize="45" fontFamily="Verdana"
                x="50" y="86">Bijutieria ADA</text>
            </svg>

            <this.toRender/>

            <this.tabsToRender/>    
            
            <div className="messages">
                {messagesList}
            </div>
        </div>
    )
}

}
export default FAQSuport;