import React from 'react';
import axios from 'axios';
import { NavLink} from 'react-router-dom';
import Product from '../Product';

const taburiNelogat = [  { name: "Acasă",
                            link: "/"},
                        { name: "Înregistrare",
                            link: "/register"},
                        { name: "Intră în cont",
                            link: "/login"}
                        ];


const taburiAdmin = [  { name: "Acasă",
                            link: "/"},
                            { name: "Magazin",
                            link: "/products"},
                            { name: "Administrare",
                            link : "/admin"}
                            ];


const taburiUser = [  { name: "Acasă",
                        link: "/"},
                        { name: "Magazin",
                        link: "/products"},
                        ];


class Administration extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            items: [],
            name: '',
            category: '',
            stock: 0,
            price: 0,
            img: '',
            currentUser: "",
            isUserLogegd: false
        };
    }

    componentDidMount() {
        this.getProducts();

      }

    onLogout = () => {
        localStorage.clear();
        this.props.history.push('/');
        this.props.history.go();
    };

    tabsToRender = () => {  
        if (localStorage.getItem('userLogged') != null) {
           if (localStorage.getItem('userLogged').localeCompare("admin") === 0) {
                return (
                <ul id = "taburi">
                    {taburiAdmin.map(function(item) {
                    return <li key={item.name}>{<NavLink to={item.link}>{item.name}</NavLink>}</li>
                    })}
                </ul>)
    
            } else {
                return (
                <ul id = "taburi">
                    {taburiUser.map(function(item) {
                    return <li key={item.name}>{<NavLink to={item.link}>{item.name}</NavLink>}</li>
                    })}
                </ul>)
            }
        } else {
            return (
            <ul id = "taburi">
                {taburiNelogat.map(function(item) {
                return <li key={item.name}>{<NavLink to={item.link}>{item.name}</NavLink>}</li>
                })}
            </ul>)
    }
}



    toRender = () => {
        if (localStorage.getItem('userLogged') === null) {
            return <h2 className="left"> Intra in cont pentru cumparaturi :) </h2>
    
        }
        return <div className="hello">
        <h3 className="left">Hello, {localStorage.getItem('userLogged')}!</h3>
        <button type='button' className="right" onClick={this.onLogout}>Logout</button>
        </div>
    }

    handleInput = event => {
        const { name, value } = event.target;
    
        this.setState({
          [name]: value,
        });
    };

    getProducts = () => {
        axios.get('http://localhost:3000/api/v1/products', {
            headers: {
                Authorization: `Bearer: ${localStorage.getItem('token')}`
            }
        })
        .then(response => {
            console.log(response.data)
            this.setState({ items: response.data });
        })
        .catch(error => console.log(error));
    };

    submit = () => {
        axios.post('http://localhost:3000/api/v1/products',
        {
            name: this.state.name,
            category: this.state.category,
            stock: this.state.stock,
            price: this.state.price,
            img: this.state.img
        },
        {
            headers: {
                Authorization: `Bearer: ${localStorage.getItem('token')}`
            }
        })
        .then(response => {
            this.getProducts();
        })
        .catch(error => { alert(error.message) });
    };

    deleteProduct(productId) {
        axios.delete('http://localhost:3000/api/v1/products/' + productId, {
            headers: {
                Authorization: `Bearer: ${localStorage.getItem('token')}`
            }
        })
        .then(response => {
            this.getProducts();
        })
        .catch(error => { alert(error.message) });
    };

    

    render() {
        return (
            <div>
                <svg className="logo" height="130" width="400">
                    <defs>
                        <linearGradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
                        <stop offset="0%" id="stop-o1"/>
                        <stop offset="100%" id="stop-o2"/>
                        </linearGradient>
                    </defs>
                    <ellipse cx="200" cy="70" rx="185" ry="55" fill="url(#grad1)" />
                    <text fill="#ffffff" fontSize="45" fontFamily="Verdana"
                    x="50" y="86">Bijutieria ADA</text>
                </svg>    
                <this.toRender/>

                <this.tabsToRender/>
                <table>
                    <thead>
                        <tr>
                            <th>Nume</th>
                            <th>Categorie</th>
                            <th>Stoc</th>
                            <th>Pret</th>
                            <th>Sterge acest produs</th>
                        </tr>
                    </thead>
                    {this.state.items.map(item => 
                        <tbody key={item._id}><Product info={item} deleteProduct={this.deleteProduct}/></tbody>)}
                </table>
                <br/>
                <form onSubmit={this.submit}>
                    <fieldset>
                        <legend>Adauga un produs nou</legend>
                        <label>Nume: </label>
                        <input type='text' name="name" value={this.state.name} onChange={this.handleInput}/>
                        <label>Categorie: </label>
                        <input type='text' name="category"  value={this.state.category} onChange={this.handleInput}/>
                        <label>Stoc: </label>
                        <input type='text' name="stock" value={this.state.stock} onChange={this.handleInput}/>
                        <label>Pret: </label>
                        <input type='text' name="price"  value={this.state.price} onChange={this.handleInput}/>
                        <label>Imagine: </label>
                        <input type='text' name="img"  value={this.state.img} onChange={this.handleInput}/>
                        <br/>
                        <input classname="submit" type="submit" value="Submit"/>
                    </fieldset>
                </form>
            </div>
        );
    };
}
    

export default Administration;

