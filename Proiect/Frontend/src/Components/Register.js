import React from 'react';
import axios from 'axios';
import { NavLink} from 'react-router-dom';


const taburiNelogat = [  { name: "Acasă",
                            link: "/"},
                        { name: "Înregistrare",
                            link: "/register"},
                        { name: "Intră în cont",
                            link: "/login"}
                        ];


const taburiAdmin = [  { name: "Acasă",
                            link: "/"},
                            { name: "Magazin",
                            link: "/products"},
                            { name: "Administrare",
                            link : "/admin"}
                            ];


const taburiUser = [  { name: "Acasă",
                        link: "/"},
                        { name: "Magazin",
                        link: "/products"},
                        ];


class Register extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            name : "",
            address : "",
            currentUser: "",
            isUserLogegd: false
        };
    }

    onLogout = () => {
        localStorage.clear();
        this.props.history.push('/');
        this.props.history.go();
    };

    tabsToRender = () => {  
        if (localStorage.getItem('userLogged') != null) {
           if (localStorage.getItem('userLogged').localeCompare("admin") === 0) {
                return (
                <ul id = "taburi">
                    {taburiAdmin.map(function(item) {
                    return <li key={item.name}>{<NavLink to={item.link}>{item.name}</NavLink>}</li>
                    })}
                </ul>)
    
            } else {
                return (
                <ul id = "taburi">
                    {taburiUser.map(function(item) {
                    return <li key={item.name}>{<NavLink to={item.link}>{item.name}</NavLink>}</li>
                    })}
                </ul>)
            }
        } else {
            return (
            <ul id = "taburi">
                {taburiNelogat.map(function(item) {
                return <li key={item.name}>{<NavLink to={item.link}>{item.name}</NavLink>}</li>
                })}
            </ul>)
    }
}

    toRender = () => {
        if (localStorage.getItem('userLogged') === null) {
            return <h2 className="left"> Intra in cont pentru cumparaturi :) </h2>
    
        }
        return <div className="hello">
        <h3 className="left">Hello, {localStorage.getItem('userLogged')}!</h3>
        <button type='button' className="right" onClick={this.onLogout}>Logout</button>
        </div>
    }

    handleInput = event => {
        const { name, value } = event.target;
    
        this.setState({
          [name]: value,
        });
    };


    register = () => {
        axios.post('http://localhost:3000/api/v1/users/register', {
            username: this.state.username,
            password: this.state.password,
            name: this.state.name,
            address: this.state.address
        })
        .then(response => {
        })
        .catch(error => console.log(error));
        this.props.history.push('/login');
        this.props.history.go();
    };

    render() {
        return (
            
            <div className="Authentication">
                <svg className="logo" height="130" width="400">
                    <defs>
                        <linearGradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
                        <stop offset="0%" id="stop-o1"/>
                        <stop offset="100%" id="stop-o2"/>
                        </linearGradient>
                    </defs>
                    <ellipse cx="200" cy="70" rx="185" ry="55" fill="url(#grad1)" />
                    <text fill="#ffffff" fontSize="45" fontFamily="Verdana"
                    x="50" y="86">Bijutieria ADA</text>
                </svg>    
                <this.toRender/>

                <this.tabsToRender/>
            <h1>Inregistrare</h1>
                <form>
                    <div>
                        <label>
                            <b>Nume si prenume</b>
                            <input type="text" className="login-input" name="name" value={this.state.name} 
                            onChange={this.handleInput}/>
                        </label>
                    </div>
                    <div>
                        <label>
                            <b>Adresa email</b>
                            <input type="text" name="address" className="login-input" value={this.state.address}
                            onChange={this.handleInput}/>
                        </label>
                    </div>
                    <div>
                        <label>
                            <b>Nume utilizator</b>
                            <input type="text" className="login-input" name="username" value={this.state.username} 
                            onChange={this.handleInput}/>
                        </label>
                    </div>
                    <div>
                        <label>
                            <b>Parola</b>
                            <input type="password" name="password" className="login-input" value={this.state.password}
                            onChange={this.handleInput}/>
                        </label>
                    </div>
                    <button type='button' className="button-basic" onClick={this.register}>Register</button>
                </form>
            </div>
        )
    }
}

export default Register;
