import React from 'react';
import {NavLink} from 'react-router-dom';
import image1 from '../images/207.jpg';
import image2 from '../images/start2.jpg';
import image3 from  '../images/start3.JPG';

const taburiNelogat = [  { name: "Acasă",
                            link: "/"},
                        { name: "Înregistrare",
                            link: "/register"},
                        { name: "Intră în cont",
                            link: "/login"}
                        ];


const taburiAdmin = [  { name: "Acasă",
                            link: "/"},
                            { name: "Magazin",
                            link: "/products"},
                            { name: "Administrare",
                            link : "/admin"}
                            ];


const taburiUser = [  { name: "Acasă",
                        link: "/"},
                        { name: "Magazin",
                        link: "/products"},
                        { name: "Contact",
                          link: "/contact"},
                        {name: "FAQ",
                        link: "/faq"}
                        ];

const taburiSuport = [ { name: "FAQ-Suport",
                        link : "/faqSuport"
                        },
                        { name: "Acasă",
                        link: "/"},
            ];

const program = ['Luni-Vineri: 10:00 - 22:00', 'Sâmbătă: 10:00 - 19:00', 'Duminică: 10:00 - 15:00'];

const contact = ['Adresă: Bdul. Vasile Milea nr.4 Sector 6 Bucureşti', 'Email: bijuteriaada@gmail.com', 'Telefon: 0745786589'];

class Home extends React.Component {

    onLogout = () => {
        localStorage.clear();
        this.props.history.push('/');
        this.props.history.go();
    }

    
    tabsToRender = () => {  
        if (localStorage.getItem('userLogged') != null) {
           if (localStorage.getItem('userLogged').localeCompare("admin") === 0) {
                return (
                <ul id = "taburi">
                    {taburiAdmin.map(function(item) {
                    return <li key={item.name}>{<NavLink to={item.link}>{item.name}</NavLink>}</li>
                    })}
                </ul>)
            } else if (localStorage.getItem('userLogged').localeCompare("suport") === 0) {
            return (
                <ul id = "taburi">
                    {taburiSuport.map(function(item) {
                    return <li key={item.name}>{<NavLink to={item.link}>{item.name}</NavLink>}</li>
                    })}
                </ul>)
            } else {
                return (
                <ul id = "taburi">
                    {taburiUser.map(function(item) {
                    return <li key={item.name}>{<NavLink to={item.link}>{item.name}</NavLink>}</li>
                    })}
                </ul>)
            }
            
        } else {
            return (
            <ul id = "taburi">
                {taburiNelogat.map(function(item) {
                return <li key={item.name}>{<NavLink to={item.link}>{item.name}</NavLink>}</li>
                })}
            </ul>)
    }
}
    


toRender = () => {
    if (localStorage.getItem('userLogged') === null) {
        return <h2 className="left"> Intra in cont pentru cumparaturi :) </h2>

    }
    return <div className="hello">
    <h3 className="left">Hello, {localStorage.getItem('userLogged')}!</h3>
    <button type='button' className="right" onClick={this.onLogout}>Logout</button>
    </div>
}


render(){
  return (
    <div>
    <svg className="logo" height="130" width="400">
                    <defs>
                        <linearGradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
                        <stop offset="0%" id="stop-o1"/>
                        <stop offset="100%" id="stop-o2"/>
                        </linearGradient>
                    </defs>
                    <ellipse cx="200" cy="70" rx="185" ry="55" fill="url(#grad1)" />
                    <text fill="#ffffff" fontSize="45" fontFamily="Verdana"
                    x="50" y="86">Bijutieria ADA</text>
                </svg>
    <this.toRender/>

    <this.tabsToRender/>
     
    <img src={image1} alt="Start" className="start"/>

    <img src={image2} alt="Start2" className="start2"/>

    <img src={image3} alt="Start3" className="start3"/>

    <h1 id="noutati"> Noutăți </h1>

    <h2>Combină și asortează în propriul stil </h2>

    <p>Asigură-te că anul 2020 este anul în care îți dezvolți stilul propriu prin combinațiile și suprapunerile 
    de bijuterii. Descoperă cum poți scoate din anonimat orice outfit prin combinații de bijuterii care spun 
    povestea stilului tău. Deși moda suprapunerii de bijuterii presupune propriile reguli stilistice, am adunat 
    câteva recomandări care-ți pot fi de folos la început.</p>
 
    <h2>Suprapuneri de brățări şi talismane</h2>
    <p>Suprapuneri de brățări şi talismane

    Aceasta este una dintre cele mai simple metode prin care poți începe să suprapui bijuterii și probabil deții
    deja o brățară cu numeroase talismane, așa că ești deja la jumătatea drumului! Mai departe, încearcă să
      suprapui mai multe brățări cu talismane - accentul se pune pe contrastul dintre forme și culori.</p>
    <h2>Inele suprapuse</h2>
    <p>Inelele pot fi suprapuse pe un singur deget sau pe ambele mâini. Colecția ADA include o selecție de
     inele ce pot fi suprapuse, create pentru a se potrivi perfect, însă toate inelele noastre sunt realizate
    ținând cont de posibilitatea de suprapunere a acestora, așa încât este la latitudinea ta să alegi cum 
    le porți.</p>

    <h1 id="program"> Program </h1>
    <ul id="lista">
      {program.map(function(item) {
         return <li key={item}>{item}</li>;
      })}
    </ul>

    <h1 id="contact"> Contact </h1>
    <ul id="lista">
      {contact.map(function(item) {
         return <li key={item}>{item}</li>;
      })}
    </ul>
    </div>
  );
}
}
export default Home;
