import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrash } from '@fortawesome/free-solid-svg-icons'

function Product(props){
    return (  
        <tr>
            <td>{props.info.name}</td>
            <td>{props.info.category}</td>
            <td>{props.info.stock}</td>
            <td>{props.info.price}</td>
            <td><button type="button" className="icon" onClick={() => {props.deleteProduct(props.info._id)}}>
                <FontAwesomeIcon icon={faTrash}/>
            </button></td>

        </tr>
    );
};

export default Product;
