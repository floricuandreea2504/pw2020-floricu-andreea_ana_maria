import React, {useState, useEffect} from 'react';
import './App.scss';
import logo from './logo.svg';

function Counter({count, Increment, Decrement, Reset}) {

  useEffect(() => {
    if (count === 0) {
      alert("Counter : 0 !!!")
    }
  })

  return (
    <div>
      <h2> Counter : {count}</h2>
      <button onClick={Increment}>Increment</button>
      <button onClick={Decrement}>Decrement</button>
      <button onClick={Reset}>Reset</button>
    </div>
  );
}

function Layout(props) {
  return <div className="layout">{props.children}</div>
}

function Header(props) {
  return <header className="header"></header>;
}

function Nav(props) {
  return <nav className="nav">
  <div className="right">
    <select>
      <option value="" selected disabled hidden>My account</option>
      <option>Logout</option>
    </select>

  </div>
  <div className="left">
    <img src={logo} alt='logo' />
    <br/>
    <a>Home</a>
    <br/>
    <a>Books</a>
    <br/>
    <a>Authors</a>
  </div>
  </nav>;
}

function Footer(props) {
  return <div>
    <footer className="footer">
    <p>&copy; Copyright Floricu Andreea-Ana-Maria</p>
    <p>Facultatea de Automatica si Calculatoare</p>
    <p>An absolvire: 2020</p>
    </footer>
  </div>;
}

function App() {

  const [count, setCount] = useState(0);

  const Increment = () => setCount(count + 1);
  const Decrement = () => setCount(count - 1);
  const Reset = () => setCount(0);

  return (
      <Layout>
        <Counter count = {count}
                  Increment = {Increment}
                  Decrement = {Decrement}
                  Reset = {Reset}>
            </Counter>
        <Header></Header>
        <Nav></Nav>
        <Footer></Footer>       
      </Layout>
  );
}

export default App;
