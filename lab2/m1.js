var x = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

function suma (x) {
       return x.reduce((a, b) => a + b, 0);
}

console.log(suma(x));

  module.exports = {
    suma
  };