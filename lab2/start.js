const express = require('express');
const m1 = require('./m4.js');
 
const app = express();
 
app.get('/', (req, res) => {
    res.send(m1.data());
});
 
app.listen(3000);