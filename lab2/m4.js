const moment = require('moment');

function data () {
    return moment().format("YYYY-LL-ZZThh:mm");
}

module.exports = {
    data
};