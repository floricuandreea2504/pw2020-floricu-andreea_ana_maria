const express = require('express');
const router = express.Router();
const database = require('./database.js');
const bodyParser = require('body-parser');

router.use(bodyParser.json());

// inserarea unei carti
router.post('/', function(req, res) {
    var book = req.body;
    console.log(book);
    database.insertIntoDb(book);
    res.status(200).send("Book added!");
});

// afisarea tuturor cartilor sau a unei carti dupa autor dat ca parametru de cerere
router.get('/', (req, res) => {

    if(!Object.keys(req.query).length){
        res.status(200).send(JSON.stringify(database.getAllFromDb()));
        console.log(JSON.stringify(database.getAllFromDb()));
    } else {
        const author = req.query.author;
        res.status(200).send(JSON.stringify(database.getFromDbByAuthor(author)));
        console.log(JSON.stringify(database.getFromDbByAuthor(author)));
    }
});

// afisarea unei carti dupa id dat ca parametru de cale
router.get('/:id', (req, res) => {
    const bookId = req.params.id;
    try {
        res.status(200).send(JSON.stringify(database.getFromDbById(bookId)));
        console.log(JSON.stringify(database.getFromDbById(bookId)));
    } catch (err) {
        res.status(404).send(err.message);
        console.log(err.message);
    }

});

// actualizarea unei carti dupa id
router.put('/:id', function(req, res) {
    var book = req.body;
    const bookId = parseInt(req.params.id);
    console.log(book);
    try {
        database.updateById(bookId, book);
    } catch(err){
        res.status(404).send(err.message);
        console.log(err.message);
    }
    res.status(201).send("Book updated!");
    console.log("Book updated!");

});

// stergerea unei carti dupa id dat ca parametru de cale
router.delete('/:id', (req, res) => {
    const bookId = req.params.id;
    database.removeFromDbById(bookId);
    res.status(200).send('Book deleted');
    console.log('Book deleted');

});

// stergerea mai multor carti dupa autor dat ca parametru de cerere sau stergerea intregii baze de date
router.delete('/', (req, res) => {

    if(!Object.keys(req.query).length){
        database.purgeDb();
        res.send('All books deleted');
        console.log('All books deleted');

    } else {
        const author = req.query.author;
        database.removeFromDbByAuthor(author);
        res.send(`The book with author ${author} has been deleted`);
        console.log(`The book with author ${author} has been deleted`);

    }
});

module.exports = router;
