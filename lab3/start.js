const express = require('express');
const myAwesomeRoute = require('./route.js');
 
const app = express();
 
app.use('/books', myAwesomeRoute);
 
app.listen(3001, () => {console.log('App listening on port 3001');});