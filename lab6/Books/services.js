const {
    Books
} = require('../data');

const Authors = require('../Authors/services');

const add = async (name, authorId, genres) => {
    let author = authorId;
    const book = new Books({
        author,
        name,
        genres
    });

    console.log(book);
    await book.save();
};

const getAll = async () => {
    let books = await Books.find().populate('author');

    return books.map(book => {
        return {
            _id: book._id,
            name: book.name,
            author: {
                firstName: book.author.firstName,
                lastName: book.author.lastName
            },
            genres: book.genres
        }
    });

};

const getById = async (id) => {
    const book = await Books.findById(id).populate('author');

    if (book === null)
        return [];

    const finalBook = {
        _id: book._id,
        name: book.name,
        author: {
            firstName: book.author.firstName,
            lastName: book.author.lastName
        },
        genres: book.genres
    }

    return finalBook;
};

const getByAuthorId = async (id) => {
    let books = await Books.find({"author" : id}).populate('author');

    return books.map(book => {
        return {
            _id: book._id,
            name: book.name,
            author: {
                firstName: book.author.firstName,
                lastName: book.author.lastName
            },
            genres: book.genres
        }
    });
};

const updateById = async (id, name, author, genres) => {
    await Books.findByIdAndUpdate(id, {name, author, genres});
};

const deleteById = async (id) => {
    await Books.findByIdAndDelete(id);
};

module.exports = {
    add,
    getAll,
    getById,
    getByAuthorId,
    updateById,
    deleteById
}